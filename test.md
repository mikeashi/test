# Product Backlog

Hier werden **alle** Anforderungen in Form von **User Stories** geordnet aufgelistet.

## Epic 1 *Eventverwaltung*

> Als *Spielleiter* möchte ich *Events bearbeiten*, um *Neue Events zu erstellen, bestehende zu bearbeiten und zu löschen*.

Ausführliche Beschreibung: 

Bei der Eventverwaltung wird das Verwalten der einzelnen Datenbereiche (Events, Rätsel, Gruppen) beschrieben.
Der Spielleiter verwaltet diese Daten durch ein spezielles Menü in der Nutzeroberfläche auf seinem Endgerät.
Nach der Anmeldung durch den Spielleiter erscheint ein Menü durch das er Zugriff auf die diversen Verwaltungsbereiche bekommt.
In der Verwaltungsoberfläche können jeweils Rätsel, Rätsellisten, Gruppen und Eventlisten angelegt und zusätzlich verwaltet werden.
Zudem können diese verwalteten Objekte in Listen eingefügt werden und Relationen zu anderen Objekten erzeugt werden.
Nach der Verwaltung lassen sich diese Objekte exportieren um diese an andere Teilnehmer verteilen zu können.

### Feature 1.1 *Anmeldung*

> Als *Spielleiter* möchte ich *mich anmelden können*, um *auf die Eventverwaltung zugreifen zu können*.

- Aufwandsschätzung: S
- Akzeptanztests:
  - Die Anmeldung des Spielleiters ist mit Benutzername und Passwort möglich.
  - Die Anmeldung ermöglicht dem Spielleiter die Verwaltung.
  - Die Anmeldung ist persistent bis die Anwendung entweder komplett beendet wird oder der Spielleiter sich abmeldet.

### Feature 1.2 *Rätselverwaltung*

> Als *Spielleiter* möchte ich *neue Rätsel anlegen*, um *diese in Rätsellisten zusammen zu führen*.

- Aufwandsschätzung: XL
- Akzeptanztests:
  - Rätsel einsehen,anlegen,löschen,bearbeiten.
  - Es können Rätsel mit einer Frage und einer Antwort verwaltet werden.
  - Es können Aufgabenstellungen in Form von Bildern, Quell-Code-Text, Fragen, Freitext bearbeitet werden.
  - Es können Lösungsmöglichkeiten in Form von GPS-Koordinaten, Antworten bearbeitet werden.
  - Rätsel können ohne Programmierkenntnisse oder bauen (Compilieren o. ä.) der Anwendung verwaltet werden.
  - Nur ein Spielleiter kann Rätsel verwalten.
  - Rätsel werden persistent gespeichert.
  - Zeitlicher Richtwert für jedes Rätsel angeben
  - Verschiedene Lösungsmöglichkeiten für eine Frage

### Feature 1.3 *Gruppenverwaltung*

> Als *Spielleiter* möchte ich *Gruppen verwalten*, um *diese einem Event zuordnen zu können*.

- Aufwandsschätzung: M
- Akzeptanztests:
  - Es können Gruppen verwaltet (neu angelegt, bearbeitet, angesehen und gelöscht) werden.
  - Anmeldedaten der Gruppe einsehen und abändern (Gruppenname und Passwort).
  - Nur ein Spielleiter kann Gruppen verwalten.
  - Gruppen werden persitent gespeichert.
  - Gruppenpasswort wird verschlüsselt gespeichert.

### Feature 1.4 *Rätsellisten*

> Als *Spielleiter* möchte ich *Rätsellisten verwalten*, um *diese einem Event zuordnen zu können*.

- Aufwandsschätzung: M
- Akzeptanztests:
  - Rätzel hinzufügen/entfernen.
  - Listen erstellen/löschen.
  - Liste umbenennen.
  - Rätsel-Listen können ohne Programmierkenntnisse oder Bauen (Compilieren o. ä.) der Anwendung verwaltet werden.
  - Rätsel können einer oder mehreren Rätsel-Listen zugeordnet werden.
  - Nur ein Spielleiter kann Rätsellisten verwalten.
  - Rätsel-Listen werden persistent gespeichert.
  - Reihenfolge der Rätsel ist konfigurierbar.
  - Rätselliste kann zufällig permutiert werden

### Feature 1.5 'Eventlisten'

> Als *Spielleiter* möchte ich *Eventlisten verwalten*, um *verschiedene Events organisieren zu können*.

- Aufwandsschätzung: S
- Akzeptanztests:
  - Event hinzufügen.
  - Event löschen.
  - Event umbenennen.
  - Gruppen hinzufügen/entfernen.
  - Rätsellisten hinzufügen/entfernen.
  - Es kann genau eine Rätsel-Liste einer Gruppe zugeordnet werden.

### Feature 1.6 'Importieren/Exportieren von Spieldaten'

> Als *Spielleiter* möchte ich *Dateien importieren und exportieren*, um *diese zu verwalten und verteilen zu können*.

- Aufwandsschätzung: L
- Akzeptanztests:
    - Teilnehmer kann Event importieren (NFC, etc)
     - Spielleiter kann Eventlisten exportieren/importieren
     - Exportierte Daten werden verschlüsselt


## Epic 2 *Eventausführung*

> Als *Spielleiter* möchte ich *Events durchführen*.

Ausführliche Beschreibung:

Bei der Eventausführung kann der Spielleiter das Event für die Teilnehmer freigeben sowie pausieren und stoppen.
Beim Start des Events wird den Teilnehmern der Zugriff auf das erste Rätsel sowie weiteren erforderlichen Resourcen gewährt.
Zudem besteht für den Spielleiter die Möglichkeit den Spielfortschritt aller teilnehmenden Gruppen einzusehen.
Um während dem laufenden Spiel den Gruppen Nachrichten senden zu können, besteht für den Spielleiter die Möglichkeit eine Broadcast
Nachricht zu schicken. Dabei wird an jede Gruppe diese Nachricht gesendet und wird auf dem Bildschrim angezeigt.

### Feature 2.1 *Kontrollfluss*

> Als *Spielleiter* möchte ich *Events kontrollieren*, um *die Bearbeitung der Rätsel für die Gruppen freizuschalten oder zu sperren*.

- Aufwandsschätzung: M
- Akzeptanztests:
  - Bei der Auswahl der jeweiligen Kontrollfunktion wird das Spiel in den entsprechenden Zustand versetzt.
  - Beim Eintritt in einen neuen Zustand wird der Eventtimer gestartet/pausiert/gestoppt.
  - Beim Eintritt in einen neuen Zustand wird das Lösen der Rätsel gesperrt/freigegeben.
  - Den Gruppen wird der aktuelle Zustand angezeigt.

#### Implementable Story 2.1.1 *Events starten*

> Als *Spielleiter* möchte ich ein Event starten können, um *die Bearbeitung der Rätsel für die Gruppen freizuschalten*.

- Aufwandsschätzung: [0-100] Story Points
- Akzeptanztests:
  - Der Eventtimer muss gestartet werden.
  - Das Lösen der Rätsel muss freigeschaltet werden.

##### Task 2.1.1.1 *Events-zustand kontroller UI*
- Aufwandsschätzung: [0.15] Stunden

##### Task 2.1.1.2 *Der Start-zustand wird an allen Gruppen gezeigt*
- Aufwandsschätzung: [0.10] Stunden

#### Implementable Story 2.1.2 *Events stoppen*

> Als *Spielleiter* möchte ich ein Event stoppen können, um *die Bearbeitung der Rätsel für die Gruppen zu sperren*.

- Aufwandsschätzung: [0-100] Story Points
- Akzeptanztests:
  - Das Lösen der Rätsel muss gesperrt werden können und wird dann auch tatsächlich gesperrt.
  - Der Eventtimer muss gestoppt werden. 

##### Task 2.1.2.1 *Überprüfen ob das Event nicht schon gestoppt ist*
- Aufwandsschätzung: [0.05] Stunden

##### Task 2.1.2.2 *Der Stopp-zustand wird an allen Gruppen gezeigt*
- Aufwandsschätzung: [0.10] Stunden

### Feature 2.2 *Spielfortschritt einsehen*

> Als *Spielleiter* möchte ich *die Gruppen des Events überwachen*, um *deren Forschritt zu beobachten*.

- Aufwandsschätzung: M
- Akzeptanztests:
  - Fortschritt aller Gruppen wird grafisch dargestellt.
  - Der Spielfortschritt einer Gruppe kann grafisch dargestellt werden.

#### Implementable Story 2.2.1 *Forschritt grafisch darstellen*

> Als *Spielleiter* möchte ich *den Forschritt aller Gruppen als Graph einsehen*.

- Aufwandsschätzung: [0-100] Story Points
- Akzeptanztests:
  - den Forschritt jeder Gruppe wird als Funktion der Anzahl der Korrekten Lösungen Graphisch dargestellt.
  - der Spielfortschritt einer Gruppe kann betrachtet werden.

##### Task 2.2.1.1 *Forschritt aller Gruppen anzeigen*
- Aufwandsschätzung: [0.05] Stunden


##### Task 2.2.1.2 *Forschritt einer Gruppe anzeigen*
- Aufwandsschätzung: [0.05] Stunden

### Feature 2.3 *Broadcastnachrichten versenden*

> Als *Spielleiter* möchte ich *Broadcastnachrichten versenden*, um *Gruppen zu informieren*.

- Aufwandsschätzung: S
- Akzeptanztests:
    - Nachricht wird allen Gruppen angezeigt.

## Epic 3 *Eventteilnahme*

> Als *Teilnehmer* möchte ich *bei Events teilnehmen*, um *Rätsellisten zu lösen*.

Ausführliche Beschreibung:

Bei der Eventteilnahme kann ein einzelner Benutzer sich einer Gruppe anschließen, die sich an dem Event anmelden kann.
Nach erfolgreicher Anmeldung können Rätsel importiert werden und mit dem Lösen der Rätel begonnen werden. In einer zukünfigen
Version des Endprodukts soll zusätzlich die Möglichkeit bestehen, den Spielfortschritt anderer Gruppen für jede Gruppe sichtbar
zu machen. Die Rätsel werden in einer vorgegebenen Reihenfolge von den jeweiligen Gruppen abgearbeitet.


### Feature 3.1 *Anmeldung*

> Als *Teilnehmer* möchte ich *mich in meiner Gruppe anmelden können*, um *zugriff auf die Rätsel zu erhalten*.

- Aufwandsschätzung: S
- Akzeptanztests:
    - Überprüfung der Anmeldedaten.
    - bei erfolgreicher Anmeldung und begonnenem Event wird die Rätseloberfläche angezeigt.
    - bei erfolgreicher Anmeldung und noch nicht begonnenem Event wird eine Warteoberfläche angezeigt.
    - bei unerfolgreicher Anmeldung Benachrichtigung anzeigen.
    - Die Anmeldung ist persistent bis die Anwendung entweder komplett beendet wird oder der Benutzer sich abmeldet.


### Feature 3.2 *Rätsel lösen*

> Als *Teilnehmer* möchte ich *die Rätsel des Events lösen*, um *die Rätselliste abzuarbeiten*.

- Aufwandsschätzung: L
- Akzeptanztests:
    - Die Bearbeitung von Rätsel ist nur möglich mit erfolgreicher Anmeldung.
    - Ein Rätsel stellt immer die Fragestellung (evtl. inkl. Medien) dar und bietet die Möglichkeit zur Beantwortung des Rätsel in der gleichen Ansicht.
    - Es ist nur das aktuelle, als nächstes zu lösende, Rätsel sichtbar.
    - Als Lösungseingabe kann Text eingegeben, eine Single-Choice antwort, ein GPS-Koordinatenbereich erreicht werden oder ein QR-Code gescannt werden.
    - Bei richtiger Lösungseingabe wird das nächste Rätsel angezeigt.
    - Der aktuelle Fortschritt der Rätsel-Liste (welches Rätsel wurde gelöst) wird persistent gespeichert.
    - Zeitstrafe bei zu vielen falschen Antworten
    - Rätsel kann übersprungen werden aber nicht mehr zurück gekehrt werden

### Feature 3.3 *Spielfortschritt aller Gruppen anzeigen*

> Als 'Teilnehmer' möchte ich den Spielfortschritt anderer Gruppen einsehen.

- Aufwandsschätzung: M
- Akzeptanztests:
    - Anzeigen des Fortschritts anderer Gruppen